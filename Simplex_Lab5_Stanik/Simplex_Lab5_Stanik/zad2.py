import logging
from saport.simplex.model import Model 
from saport.simplex.analyser import Analyser

def run():
    primal = Model("zad2")

    l = primal.create_variable("l")
    s = primal.create_variable("s")
    k = primal.create_variable("k")

    primal.add_constraint(8*l + 6*s + 1*k <= 960)
    primal.add_constraint(3*l + 4*s + 3*k <= 800)
    primal.add_constraint(4*l + 3*s + 1*k <= 320)
    
    primal.maximize(60*l + 30*s + 20*k)

    dual = primal.dual()
    
    print(dual)
    
    assert primal.is_equivalent(dual.dual()), "double dual should equal the initial model"
    
    primal_solution = primal.solve()
    dual_solution = dual.solve()
    
    print(primal)
    print("Solution:\n", primal_solution)
    
    analyser = Analyser()
    analysis_results = analyser.analyse(primal_solution)
    analyser.interpret_results(primal_solution, analysis_results, logging.info)

    #ominiecie roznic wynikajacych z bledow zaokraglenia w jednym z rozwiazan
    assert primal_solution.objective_value() - dual_solution.objective_value() < 0.000001, "dual and primal should have the same value at optimum"
    

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()