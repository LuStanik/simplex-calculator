import logging
from saport.simplex.model import Model 

def run():
    primal = Model("example_06_dual")

    x1 = primal.create_variable("x1")
    x2 = primal.create_variable("x2")
    x3 = primal.create_variable("x3")

    primal.add_constraint(8*x1 + 16*x2 - 2*x3 <= 10)
    primal.add_constraint(14*x1 - 4*x2 + 4*x3 >= 8)
    
    primal.maximize(6*x1 + 4*x2 - 12*x3)

    expected_dual = Model("example_06_dual (expected dual)")

    y1 = expected_dual.create_variable("y1")
    y2 = expected_dual.create_variable("y2")

    expected_dual.add_constraint(8*y1 - 14*y2 >= 6)
    expected_dual.add_constraint(16*y1 + 4*y2 >= 4)
    expected_dual.add_constraint(-2*y1 - 4*y2 >= -12)

    expected_dual.minimize(10 * y1 - 8 * y2)
    
    dual = primal.dual()
    
    assert dual.is_equivalent(expected_dual), "dual wasn't calculated as expected"
    assert primal.is_equivalent(dual.dual()), "double dual should equal the initial model"
    
    primal_solution = primal.solve()
    dual_solution = dual.solve()
    assert primal_solution.objective_value() - dual_solution.objective_value() < 0.01, "dual and primal should have the same value at optimum"
    
    logging.info("Congratulations! The dual creation seems to be implemented correctly :)")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
