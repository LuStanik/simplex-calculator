import logging
from saport.simplex.model import Model 
from saport.simplex.analyser import Analyser

def run():
    primal = Model("zad1")

    ss = primal.create_variable("ss")
    s = primal.create_variable("s")
    o = primal.create_variable("o")

    primal.add_constraint(2*ss + 2*s + 5*o <= 40)
    primal.add_constraint(1*ss + 3*s + 2*o <= 30)
    primal.add_constraint(3*ss + 1*s + 3*o <= 30)
    
    primal.maximize(32*ss + 24*s + 48*o)

    dual = primal.dual()
    
    print(dual)
    
    assert primal.is_equivalent(dual.dual()), "double dual should equal the initial model"
    
    primal_solution = primal.solve()
    dual_solution = dual.solve()
    
    print(primal)
    print("Solution:\n", primal_solution)
    
    analyser = Analyser()
    analysis_results = analyser.analyse(primal_solution)
    analyser.interpret_results(primal_solution, analysis_results, logging.info)

    #ominiecie roznic wynikajacych z bledow zaokraglenia w jednym z rozwiazan
    assert primal_solution.objective_value() - dual_solution.objective_value() < 0.000001, "dual and primal should have the same value at optimum"
    

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()