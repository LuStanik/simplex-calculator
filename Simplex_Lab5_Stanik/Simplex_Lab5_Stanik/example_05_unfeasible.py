import logging
from saport.simplex.model import Model 

def run():
    #TODO:
    # fill missing test based on the example_01_solvable.py
    # to make the test a bit more interesting:
    # * make the model unfeasible in a way detectable by the 2-phase simplex
    # 

    model = Model("example_05_unfeasible")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.maximize(x1 + 2*x2)
    model.add_constraint(5*x1 + 4*x2 + 3*x3 == 1)
    model.add_constraint(x1 - 10*x2 + x3 >= 213)
    
    try:
        solution = model.solve()
    except Exception:
        logging.info("Congratulations! You found an unfeasiable solution :)")
    else:
        raise AssertionError("This problem has no solution but your algorithm hasn't figured it out!")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
