import logging
from saport.simplex.model import Model


def run():
    #TODO:
    # fill missing test based on the example_01_solvable.py
    # to make the test a bit more interesting:
    # * minimize the objective (so the solver would have to normalize it)
    # * make some "=>" constraints
    # * the model still has to be solvable by the basix simplex withour artificial variables
    # 
    # TIP: you may use other solvers (e.g. https://online-optimizer.appspot.com)
    #      to find the correct solution


    model = Model("example_01_solvable")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(x1 + x2 + x3 >= -400)
    model.add_constraint(x1 + x2 >= -300)
    model.add_constraint(x1 + 2*x2 + 2*x3 <= 200)

    model.minimize(5 * x1 + 6 * x2 + 7 * x3)

    try:
        solution = model.solve()
    except:
        raise AssertionError(
            "This problem has a solution and your algorithm hasn't found it!")

    logging.info(solution)

    assert (solution.assignment == [
            0.0, 0.0, 0.0, 400.0, 300.0, 200.0]), "Your algorithm found an incorrect solution!"

    logging.info("Congratulations! This solution seems to be alright :)")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
