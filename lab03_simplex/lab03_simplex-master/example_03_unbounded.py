import logging
from saport.simplex.solver import ErrorException
from saport.simplex.model import Model 

def run():
    #TODO:
    # fill missing test based on the example_01_solvable.py
    # to make the test a bit more interesting:
    # * make the model unbounded!
    # 
    # TIP: you may use other solvers (e.g. https://online-optimizer.appspot.com)
    #      to check if the problem is unbounded
    model = Model("example_01_solvable")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(x1 + x2 >= -200)
    model.add_constraint(x2 + x3 >= -500)
    model.add_constraint(4*x1 +2* x2 + x3 >= -900)

    model.maximize(2 * x1 + 3 * x2 + 4 * x3)

    try:
        solution = model.solve()
    except ErrorException:
        print("Unbounded model!")
        
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()



