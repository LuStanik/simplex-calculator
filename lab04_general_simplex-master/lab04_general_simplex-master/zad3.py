import saport.simplex.model as m
from saport.simplex.model import Model 


model = Model("zadanie 2")

s = model.create_variable("s")
z = model.create_variable("z")

model.add_constraint(5*s + 15*z >= 50)
model.add_constraint(20*s + 5*z >= 40)
model.add_constraint(15*s + 2*z <= 60)

model.minimize(8*s + 4*z)

solution = model.solve()
print(solution)
