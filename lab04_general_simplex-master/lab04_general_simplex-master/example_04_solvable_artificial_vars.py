import logging
from saport.simplex.model import Model


def run():
    #TODO:
    # fill missing test based on the example_01_solvable.py
    # to make the test a bit more interesting:
    # * make the solver use artificial variables!
    model = Model("example_04_artificial_variables")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(2*x1 + 2*x3 == 6)
    model.add_constraint((-5.5)*x1 + 1*x3 <= -0.5)
    model.add_constraint(7*x2 + x3  <=  7)
    model.maximize(3*x1 + x2 + x3)

    try:
        solution = model.solve()
    except:
        raise AssertionError(
            "This problem has a solution and your algorithm hasn't found it!")

    logging.info(solution)

    assert (solution.assignment == [3.0, 1.0, 0.0]
            ), "Your algorithm found an incorrect solution!"

    logging.info("Congratulations! This solution seems to be alright :)")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()