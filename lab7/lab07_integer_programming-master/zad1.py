from saport.integer.model import Model 
from saport.knapsack import model

m = Model("Sense of Life")

# TODO: create variables, add constraints, set objective, etc.

dishes = ["moules marinieres", "pate de foie gras", "beluga caviar", "egg Benedictine", "wafer-thin mint", "salmon mousse"]
variables = [m.create_variable(d) for d in dishes]

prices = 2.15 * variables[0] + 2.75 * variables[1] + 3.35 * variables[2] + 3.55 * variables[3] + 4.20 * variables[4] + 5.8*variables[5] <= 50
m.add_constraint(prices)

stock = [5, 6, 7, 5, 1, 1]
for x, amount in zip(variables, stock):
    m.add_constraint(x <= amount)

objective = 3 * variables[0] + 4 * variables[1] + 4.5 * variables[2] + 4.65 * variables[3] + 8 * variables[4] + 9 * variables[5]
m.maximize(objective)

solution = m.solve()

print(m)
print(solution)