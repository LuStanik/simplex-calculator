from ..abstractsolver import AbstractSolver
from ..model import Problem, Solution, Item
from typing import List 
from ...integer.model import Model
from ...simplex.expressions.expression import Expression

class IntegerSolver(AbstractSolver):
    """
    An Integer Programming solver for the knapsack problems

    Methods:
    --------
    create_model() -> Models:
        creates and returns an integer programming model based on the self.problem
    """

    def create_model(self) -> Model:
        #TODO: create an integer programming model based on the problem
        model = Model("Knapsack problem model")
        items = self.problem.items
        variables = []
        for i in range(len(items)):
            variables.append(model.create_variable(f"x{i}"))
        weights = Expression()
        values = Expression()
        n = 0
        for variable in variables:
            weights += variable * items[n].weight
            values += variable * items[n].value
            model.add_constraint(variable <= 1)
            n+=1
        model.add_constraint(weights <= self.problem.capacity)
        model.maximize(values)
        return model


    
    def solve(self) -> Solution:
        m = self.create_model()
        integer_solution = m.solve(self.timelimit)
        items = [item for (i,item) in enumerate(self.problem.items) if integer_solution.value(m.variables[i]) > 0]
        solution = Solution.from_items(items, not m.solver.interrupted)
        self.total_time = m.solver.total_time
        return solution