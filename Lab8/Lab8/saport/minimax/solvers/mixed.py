from dataclasses import dataclass
from .solver import AbstractSolver
from ..model import Game, Equilibrium, Strategy
from ...simplex import model as lpmodel
from ...simplex import solution as lpsolution
from ...simplex.expressions import expression as expr
from ...simplex.expressions import constraint as cons
import numpy as np
from typing import Tuple, List

class MixedSolver(AbstractSolver):

    def solve(self) -> Equilibrium:
        shifted_game, shift = self.shift_game_rewards()

        # don't remove this print, it will be graded :)
        print(f"- shifted game: \n{shifted_game}")

        a_model = self.create_max_model(shifted_game)
        b_model = self.create_min_model(shifted_game)       
        a_solution = a_model.solve()
        b_solution = b_model.solve()

        a_probabilities = self.extract_probabilities(a_solution)
        b_probabilities = self.extract_probabilities(b_solution)
        
        stratA = Strategy(a_probabilities)
        stratB = Strategy(b_probabilities)
        
        tab = np.copy(self.game.reward_matrix)
        
        for i in range(tab.shape[0]):
            for j in range(tab.shape[1]):
                tab[i,j] = stratA.probabilities[i]*stratB.probabilities[j]*tab[i,j]
        
        # TODO: the correct Equilibirum instead of None
        return Equilibrium(np.sum(tab), stratA, stratB)

    def shift_game_rewards(self) -> Tuple[Game, float]:
        #TODO:
        # check if game value can be negative
        # if it's the case calculate the correct reward shift
        # to make it nonnegative
        
        shift = 0
        worst_case = np.min(self.game.reward_matrix)
        if worst_case < 0:
            shift = abs(worst_case)

        return Game(self.game.reward_matrix + shift), shift

    def create_max_model(self, game: Game) -> lpmodel.Model:
        a_actions, b_actions = game.reward_matrix.shape

        a_model = lpmodel.Model("A")

        #TODO:
        # one variable for game value
        # + as many variables as there are actions available for player A
        # sum of those variables should be equal 1
        # for each column, value - column * actions <= 0
        # maximize value variable
        
        value = a_model.create_variable('gameValue')
        a_variables = []
        
        for i in range (a_actions):
            a_variables.append(a_model.create_variable(f"A{i+1}"))
        ex = expr.Expression.from_vectors(a_variables, np.ones(len(a_variables)))
        a_model.add_constraint(cons.Constraint(ex, 1, cons.ConstraintType.EQ))
        
        for i in range(b_actions):
            factors = np.concatenate([[1],game.reward_matrix[:,i]*-1])
            ex = expr.Expression.from_vectors([value]+a_variables, factors)
            a_model.add_constraint(cons.Constraint(ex, 0, cons.ConstraintType.LE))
            
        a_model.maximize(value)
            
        return a_model

    def create_min_model(self, game: Game) -> lpmodel.Model:
        a_actions, b_actions = game.reward_matrix.shape

        b_model = lpmodel.Model("B")
        
        #TODO:
        # one variable for game value
        # + as many variables as there are actions available for player B
        # sum of those variables should be equal 1
        # for each row, value - row * actions >= 0
        # minimize value variable
        
        game_value = b_model.create_variable('gameValue')
        b_variables = []
        
        for i in range (b_actions):
            b_variables.append(b_model.create_variable(f"B{i+1}"))
        ex = expr.Expression.from_vectors(b_variables, np.ones(len(b_variables)))
        b_model.add_constraint(cons.Constraint(ex, 1, cons.ConstraintType.EQ))
        
        for i in range(a_actions):
            factors = np.concatenate([[1],game.reward_matrix[i,:]*-1])
            ex = expr.Expression.from_vectors([game_value]+b_variables, factors)
            b_model.add_constraint(cons.Constraint(ex, 0, cons.ConstraintType.GE))
            
        b_model.minimize(game_value)

        return b_model

    def extract_probabilities(self, solution: lpsolution.Solution) -> List[float]:
        return [solution.value(x) for x in solution.model.variables if not solution.model.objective.depends_on_variable(solution.model, x)]

