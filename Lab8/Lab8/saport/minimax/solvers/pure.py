from dataclasses import dataclass
from .solver import AbstractSolver
from ..model import Game, Equilibrium, Strategy
from typing import List
import numpy as np

class PureSolver(AbstractSolver):

    def solve(self) -> List[Equilibrium]:
        #TODO: basic solver finding all pure equilibriums
        #      reminder: 
        #      if max of the column is the same as min of the row
        #      it is an equilibrium
        #      in case there is no pure equilibrium - should return an empty list
        
        equilibria = []
        rows_min = np.min(self.game.reward_matrix, axis=1)
        cols_max = np.max(self.game.reward_matrix, axis=0)
        
        for i in range (len(self.game.reward_matrix)):
            for j in range (len(self.game.reward_matrix[0])):
                if(rows_min[i] == cols_max[j]):
                    eq = Equilibrium(self.game.reward_matrix[i][j], Strategy.with_action(i, len(rows_min)), Strategy.with_action(j, len(cols_max)))
                    equilibria.append(eq)
                    
        return equilibria