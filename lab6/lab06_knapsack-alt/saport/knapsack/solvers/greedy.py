from numpy.core.arrayprint import format_float_positional
from ..abstractsolver import AbstractSolver
from ..model import Problem, Solution, Item
import time


class AbstractGreedySolver(AbstractSolver):
    """
    An abstract greedy solver for the knapsack problems.

    Methods:
    --------
    greedy_heuristic(item : Item) -> float:
        return a value representing how much the given items is valuable to the greedy algorithm
        bigger value > earlier to take in the backpack
    """

    def greedy_heuristic(self, item: Item) -> float:
        raise Exception("Greedy solver requires a heuristic!")

    def solve(self) -> Solution:
        #TODO: implement the greedy solving strategy
        #      1) sort items in the problem by the self.greedy_heuristic
        #      2) take as many as you can
        #      3) remember to replace the line below :)
        
        self.start_timer()
        self.problem.items.sort(key=lambda x: self.greedy_heuristic(x), reverse=True)
        right_values, val, wght = [], 0, 0
        
        for item in self.problem.items:
            if item.weight <= self.problem.capacity - wght:
                right_values.append(item)
                wght += item.weight
                val += item.value
                
            if wght >= self.problem.capacity:
                break
        
        self.stop_timer()
        
        return Solution(items=right_values, value=val, weight=wght, optimal=False)