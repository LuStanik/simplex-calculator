from knapsack_benchmark import KnapsackBenchmark

problems = ["ks_50_0", "ks_50_1", "ks_60_0"]

benchmark = KnapsackBenchmark(problems)
benchmark.run()